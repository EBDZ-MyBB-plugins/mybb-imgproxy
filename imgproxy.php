<?php

/**
 *
 */

$plugins->add_hook('parse_message_end', 'imgproxy_parse_message_end', 15);

function imgproxy_info() {
	return array(
		'name'          => 'img fixer',
		'description'   => 'Update non https image to use duckduckgo proxy, add referrerpolicy for other; and always lazyload img.',
		'compatibility' => '18*',
		'author'        => 'Betty',
		'authorsite'    => 'https://example.net/',
		'version'       => '1.2.0',
		'codename'      => 'imgproxy',
	);
}

function imgproxy_parse_message_end($message)
{
	global $mybb;
	$bbdomain = parse_url($mybb->settings['bburl'], PHP_URL_HOST);
	$bbscheme = parse_url($mybb->settings['bburl'],PHP_URL_SCHEME);
    $message = preg_replace_callback(
		'/<img([^>]*?)src="(.*?)"(.*?)(\/?)>/',
		function ($matches) use ($bbdomain, $bbscheme){
			$url = $matches[2];
			$scheme = parse_url($url,PHP_URL_SCHEME);
			$domain = parse_url($url, PHP_URL_HOST);
			if($bbscheme == 'https' && $bbdomain != $domain && $scheme == 'http') {
				$url = "https://proxy.duckduckgo.com/iu/?u=".$url;
			}
			$complement = " {$matches[1]}{$matches[3]}";
			if($bbdomain != $domain) {
				if(strpos($complement, "referrerpolicy") === false) {
					$complement .= " referrerpolicy=\"no-referrer\"";
				}
			}
			if(strpos($complement, "loading") === false) {
				$complement .= " loading=\"lazy\"";
			}
			return "<img src=\"{$url}\"{$complement} />";
		},
		$message
	);
    $message = preg_replace_callback(
		'/<img([^>]*?)src=\'(.*?)\'(.*?)(\/?)>/',
		function ($matches) use ($bbdomain, $bbscheme){
			$url = $matches[2];
			$scheme = parse_url($url,PHP_URL_SCHEME);
			$domain = parse_url($url, PHP_URL_HOST);
			if($bbscheme == 'https' && $bbdomain != $domain && $scheme == 'http') {
				$url = "https://proxy.duckduckgo.com/iu/?u=".$url;
			}
			$complement = " {$matches[1]}{$matches[3]}";
			if($bbdomain != $domain) {
				if(strpos($complement, "referrerpolicy") === false) {
					$complement .= " referrerpolicy='no-referrer'";
				}
			}
			if(strpos($complement, "loading") === false) {
				$complement .= " loading='lazy'";
			}
			return "<img src=\"{$url}\"{$complement} />";
		},
		$message
	);
    return $message;
}
